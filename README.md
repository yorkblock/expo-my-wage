My Wage
========
-------------------------------------
![Alt text](/readme/my-wage-logo.png)
-------------------------------------

Description
--------------

My Wage is a small app written to give you an **approximation** of your pay, taxable income, superannuation and total taxes. It provides you with calculations for the year, month, fortnight and week. Also, it allows you to input your superannuation. Before I forget, this is for Australia :-).

I wrote it to **learn/explore** JavaScript and react native (Expo). I am not using any state management framework, I wanted to keep it as simple as possible. I am planning to add one in the future (I think :-)).

App Store: [Link](https://apps.apple.com/us/app/my-wage/id1494607238?ls=1)

-------------

![Alt text](/readme/my-wage.gif)

-------------
