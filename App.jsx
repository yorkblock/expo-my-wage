import React from 'react';
import { StyleSheet, View, SafeAreaView, StatusBar } from 'react-native';
import Calculator from './src/Calculator';

export default function App() {
  StatusBar.setBarStyle('dark-content', true);

  return (
    <SafeAreaView style={styles.safeArea}>
      <View style={styles.container}>
        <Calculator />
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: 'white'
  },
  container: {
    flex: 1,
    backgroundColor: 'blue'
  },
});
