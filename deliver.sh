#!/bin/zsh

set -e

npm install

expo build:ios --release-channel production

curl -o app.ipa "$(expo url:ipa)"

fastlane deliver --verbose
