import React from 'react';
import {
  AsyncStorage,
  StyleSheet,
  View
} from 'react-native';
import {
  NumericKeypad,
  updateAmount,
  updatePercentage
} from './components/common';
import {
  SalarySuperDisplay,
  SalarySuperSegmented,
  SalarySuperSegmentType,
  TaxOptionsDropDownMenu,
  TaxOptionType,
  ResultCarrousel
} from './components';
import {
  CalculatorModel,
  Period
} from './models/CalculatorModel';

const SUPERANNUATION_KEY = 'SUPERANNUATION_KEY';
const SUPERANNUATION_INCLUDED_KEY = 'SUPERANNUATION_INCLUDED_KEY';
const NO_TAX_FREE_THRESHOLD_KEY = 'NO_TAX_FREE_THRESHOLD_KEY';

class Calculator extends React.Component {
  calculator = new CalculatorModel();

  state = {
    period: 'Year',
    salary: 0,
    superannuation: 0.00,
    showSuperannuation: false,
    superannuationIncluded: false,
    noTaxFreeThreshold: false,
    salarySuperSelection: SalarySuperSegmentType.YEAR
  };

  componentDidMount() {
    this.loadSuperannuation();
    this.loadSuperannuationIncluded();
    this.loadNoTaxFreeThreshold();
  }

  loadSuperannuation = async () => {
    try {
      const superannuation = await AsyncStorage.getItem(SUPERANNUATION_KEY)
      if (superannuation !== null) {
        this.calculator.superannuation = Number(superannuation);
        this.setState({ superannuation: Number(superannuation) });
      } else {
        this.calculator.superannuation = 9.50;
        this.setState({ superannuation: 9.50 });
      }
    } catch (e) {
      console.error('Failed to load superannuation');
    }
  }

  saveSupperannuation = async (superannuation) => {
    try {
      await AsyncStorage.setItem(SUPERANNUATION_KEY, superannuation.toString())
    } catch (e) {
      console.error('Failed to store superannuation');
    }
  }

  loadSuperannuationIncluded = async () => {
    try {
      const included = await AsyncStorage.getItem(SUPERANNUATION_INCLUDED_KEY)
      if (included !== null) {
        this.calculator.isSuperIncluded = included === 'true';
        this.setState({ superannuationIncluded: included === 'true' });
      }
    } catch (e) {
      console.error('Failed to load superannuation included');
    }
  }

  saveSuperannuationIncluded = async (included) => {
    try {
      await AsyncStorage.setItem(SUPERANNUATION_INCLUDED_KEY, included.toString())
    } catch (e) {
      console.error('Failed to store superannuation included');
    }
  }

  loadNoTaxFreeThreshold = async () => {
    try {
      const included = await AsyncStorage.getItem(NO_TAX_FREE_THRESHOLD_KEY)
      if (included !== null) {
        this.calculator.isNonTaxFreeThreshold = included === 'true';
        this.setState({ noTaxFreeThreshold: included === 'true' });
      }
    } catch (e) {
      console.error('Failed to load no tax free threshold');
    }
  }

  saveNoTaxFreeThreshold = async (threshold) => {
    try {
      await AsyncStorage.setItem(NO_TAX_FREE_THRESHOLD_KEY, threshold.toString())
    } catch (e) {
      console.error('Failed to store superannuation included');
    }
  }

  result = () => {
    const { anually, monthly, fortnightly, weekly } = this.calculator.wages;

    return ({
      anually: {
        pay: anually.pay,
        taxableIncome: anually.taxableIncome,
        superannuation: anually.superannuation,
        totalTaxes: anually.totalTaxes
      },
      monthly: {
        pay: monthly.pay,
        taxableIncome: monthly.taxableIncome,
        superannuation: monthly.superannuation,
        totalTaxes: monthly.totalTaxes
      },
      fortnightly: {
        pay: fortnightly.pay,
        taxableIncome: fortnightly.taxableIncome,
        superannuation: fortnightly.superannuation,
        totalTaxes: fortnightly.totalTaxes
      },
      weekly: {
        pay: weekly.pay,
        taxableIncome: weekly.taxableIncome,
        superannuation: weekly.superannuation,
        totalTaxes: weekly.totalTaxes
      },
    });
  };

  onSelectSalarySuper(selectedSegment) {
    const { YEAR, MONTH, WEEK, DAY, SUPER } = SalarySuperSegmentType;

    switch (selectedSegment.key) {
      case YEAR:
        this.calculator.period = Period.ANUALLY;
        break;

      case MONTH:
        this.calculator.period = Period.MONTHLY;
        break;

      case WEEK:
        this.calculator.period = Period.WEEKLY;
        break;

      case DAY:
        this.calculator.period = Period.DAILY;
        break;
    }

    if (selectedSegment.key !== SUPER) {
      this.setState({
        period: selectedSegment.title,
        showSuperannuation: false,
        salarySuperSelection: selectedSegment.key
      });

      return;
    }

    this.setState({
      showSuperannuation: true,
      salarySuperSelection: selectedSegment.key
    });
  }

  onSelectTaxOption(selectedOption) {
    const { INCLUDE_SUPERANNUATION, NO_TAX_FREE_THRESHOLD } = TaxOptionType;

    switch (selectedOption.key) {
      case INCLUDE_SUPERANNUATION:
        this.setState(previousState => {
          this.calculator.isSuperIncluded = selectedOption.isSelected;
          return ({ superannuationIncluded: selectedOption.isSelected });
        });
        this.saveSuperannuationIncluded(selectedOption.isSelected.toString());
        break;

      case NO_TAX_FREE_THRESHOLD:
        this.setState(previousState => {
          this.calculator.isNonTaxFreeThreshold = selectedOption.isSelected;
          return ({ noTaxFreeThreshold: selectedOption.isSelected });
        });
        this.saveNoTaxFreeThreshold(selectedOption.isSelected.toString());
        break;
    }
  }

  onPress(key) {
    const { salary, superannuation, salarySuperSelection } = this.state;
    const { YEAR, MONTH, WEEK, DAY, SUPER } = SalarySuperSegmentType;

    switch (salarySuperSelection) {
      case SUPER:
        const newSuperannuation = updatePercentage(superannuation, key);
        this.setState(previousState => {
          this.calculator.superannuation = newSuperannuation;
          return ({ superannuation: newSuperannuation });
        });
        this.saveSupperannuation(newSuperannuation);
        break;

      case YEAR:
      case MONTH:
      case WEEK:
      case DAY:
        const newSalary = updateAmount(salary, key);
        this.setState(previousState => {
          this.calculator.salary = newSalary;
          return ({ salary: newSalary });
        });
        break;
    }
  }

  render() {
    const { period, salary, superannuation, showSuperannuation, superannuationIncluded, noTaxFreeThreshold } = this.state;
    const { INCLUDE_SUPERANNUATION, NO_TAX_FREE_THRESHOLD } = TaxOptionType;

    return (
      <View style={styles.container}>
        <SalarySuperDisplay
          period={period}
          salary={salary}
          superannuation={superannuation}
          showSuperannuation={showSuperannuation}
        />

        <SalarySuperSegmented
          onSelect={(segment) => this.onSelectSalarySuper(segment)}
        />

        <ResultCarrousel
          result={this.result()}
        />

        <TaxOptionsDropDownMenu
          onSelect={(option) => this.onSelectTaxOption(option)}
          title='Options'
          options={[
            {
              key: INCLUDE_SUPERANNUATION,
              title: 'Include Superannuation',
              isSelected: superannuationIncluded
            },
            {
              key: NO_TAX_FREE_THRESHOLD,
              title: 'No tax-free threshold',
              isSelected: noTaxFreeThreshold
            }
          ]}
          superannuationIncluded={superannuationIncluded}
          noTaxFreeThreshold={noTaxFreeThreshold}
        />

        <NumericKeypad height={240} onPress={(key) => this.onPress(key)} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    padding: 4
  }
});

export default Calculator;
