import { CalculatorModel, Period } from './CalculatorModel';

describe('Salary $120,000.00, Super 10.00% (not included)', () => {
  const calculator = new CalculatorModel();
  calculator.salary = 120000.00;
  calculator.superannuation = 10.00;

  test('Anual pay should be $85,680.00', () => {
    expect(calculator.wages.anually.pay).toBe(85680.00);
  });

  test('Monthly pay should be $7,140.00', () => {
    expect(calculator.wages.monthly.pay).toBe(7140.00);
  });

  test('Fortnightly pay should be $3,295.38', () => {
    expect(calculator.wages.fortnightly.pay).toBe(3295.38);
  });

  test('Weekly pay should be $1,647.69', () => {
    expect(calculator.wages.weekly.pay).toBe(1647.69);
  });

  test('Anual taxable income should be $120,000.00', () => {
    expect(calculator.wages.anually.taxableIncome).toBe(120000.00);
  });

  test('Monthly taxable income should be $10,000.00', () => {
    expect(calculator.wages.monthly.taxableIncome).toBe(10000.00);
  });

  test('Fortnightly taxable income should be $4,615.38', () => {
    expect(calculator.wages.fortnightly.taxableIncome).toBe(4615.38);
  });

  test('Weekly taxable income should be $2,307.69', () => {
    expect(calculator.wages.weekly.taxableIncome).toBe(2307.69);
  });

  test('Anual superannuation should be $12,000.00', () => {
    expect(calculator.wages.anually.superannuation).toBe(12000);
  });

  test('Monthly superannuation should be $1,000.00', () => {
    expect(calculator.wages.monthly.superannuation).toBe(1000.00);
  });

  test('Fortnightly superannuation should be $461.54', () => {
    expect(calculator.wages.fortnightly.superannuation).toBe(461.54);
  });

  test('Weekly superannuation should be $230.77', () => {
    expect(calculator.wages.weekly.superannuation).toBe(230.77);
  });

  test('Anual total taxes should be $34,320.00', () => {
    expect(calculator.wages.anually.totalTaxes).toBe(34320.00);
  });

  test('Monthly total taxes should be $2,860.00', () => {
    expect(calculator.wages.monthly.totalTaxes).toBe(2860.00);
  });

  test('Fortnightly total taxes should be $1,320.00', () => {
    expect(calculator.wages.fortnightly.totalTaxes).toBe(1320.00);
  });

  test('Weekly total taxes should be $660.00', () => {
    expect(calculator.wages.weekly.totalTaxes).toBe(660.00);
  });
});

describe('Salary $120,000.00, Super 10.00% (included)', () => {
  const calculator = new CalculatorModel();
  calculator.salary = 120000.00;
  calculator.superannuation = 10.00;
  calculator.isSuperIncluded = true;

  test('Anual pay should be $79,034.91', () => {
    expect(calculator.wages.anually.pay).toBe(79034.91);
  });

  test('Monthly pay should be $6,586.24', () => {
    expect(calculator.wages.monthly.pay).toBe(6586.24);
  });

  test('Fortnightly pay should be $3,039.80', () => {
    expect(calculator.wages.fortnightly.pay).toBe(3039.80);
  });

  test('Weekly pay should be $1,519.90', () => {
    expect(calculator.wages.weekly.pay).toBe(1519.90);
  });

  test('Anual taxable income should be $109,090.91', () => {
    expect(calculator.wages.anually.taxableIncome).toBe(109090.91);
  });

  test('Monthly taxable income should be $9,090.91', () => {
    expect(calculator.wages.monthly.taxableIncome).toBe(9090.91);
  });

  test('Fortnightly taxable income should be $4,195.80', () => {
    expect(calculator.wages.fortnightly.taxableIncome).toBe(4195.80);
  });

  test('Weekly taxable income should be $2,097.90', () => {
    expect(calculator.wages.weekly.taxableIncome).toBe(2097.90);
  });

  test('Anual superannuation should be $10,909.09', () => {
    expect(calculator.wages.anually.superannuation).toBe(10909.09);
  });

  test('Monthly superannuation should be $909.09', () => {
    expect(calculator.wages.monthly.superannuation).toBe(909.09);
  });

  test('Fortnightly superannuation should be $419.58', () => {
    expect(calculator.wages.fortnightly.superannuation).toBe(419.58);
  });

  test('Weekly superannuation should be $209.79', () => {
    expect(calculator.wages.weekly.superannuation).toBe(209.79);
  });

  test('Anual total taxes should be $30,056.00', () => {
    expect(calculator.wages.anually.totalTaxes).toBe(30056.00);
  });

  test('Monthly total taxes should be $2,504.67', () => {
    expect(calculator.wages.monthly.totalTaxes).toBe(2504.67);
  });

  test('Fortnightly total taxes should be $1,156.00', () => {
    expect(calculator.wages.fortnightly.totalTaxes).toBe(1156.00);
  });

  test('Weekly total taxes should be $578.00', () => {
    expect(calculator.wages.weekly.totalTaxes).toBe(578.00);
  });
});

describe('Salary $120,000.00, Super 10.00% (not included), Non Tax Free Thresholds', () => {
  const calculator = new CalculatorModel();
  calculator.salary = 120000.00;
  calculator.superannuation = 10.00;
  calculator.isNonTaxFreeThreshold = true;

  test('Anual pay should be $78,608.00', () => {
    expect(calculator.wages.anually.pay).toBe(78608.00);
  });

  test('Monthly pay should be $6,550.67', () => {
    expect(calculator.wages.monthly.pay).toBe(6550.67);
  });

  test('Fortnightly pay should be $3,023.38', () => {
    expect(calculator.wages.fortnightly.pay).toBe(3023.38);
  });

  test('Weekly pay should be $1511.69', () => {
    expect(calculator.wages.weekly.pay).toBe(1511.69);
  });

  test('Anual taxable income should be $120,000.00', () => {
    expect(calculator.wages.anually.taxableIncome).toBe(120000.00);
  });

  test('Monthly taxable income should be $10,000.00', () => {
    expect(calculator.wages.monthly.taxableIncome).toBe(10000.00);
  });

  test('Fortnightly taxable income should be $4,615.38', () => {
    expect(calculator.wages.fortnightly.taxableIncome).toBe(4615.38);
  });

  test('Weekly taxable income should be $2,307.69', () => {
    expect(calculator.wages.weekly.taxableIncome).toBe(2307.69);
  });

  test('Anual superannuation should be $12,000.00', () => {
    expect(calculator.wages.anually.superannuation).toBe(12000);
  });

  test('Monthly superannuation should be $1,000.00', () => {
    expect(calculator.wages.monthly.superannuation).toBe(1000.00);
  });

  test('Fortnightly superannuation should be $461.54', () => {
    expect(calculator.wages.fortnightly.superannuation).toBe(461.54);
  });

  test('Weekly superannuation should be $230.77', () => {
    expect(calculator.wages.weekly.superannuation).toBe(230.77);
  });

  test('Anual total taxes should be $41,392.00', () => {
    expect(calculator.wages.anually.totalTaxes).toBe(41392.00);
  });

  test('Monthly total taxes should be $3,449.33', () => {
    expect(calculator.wages.monthly.totalTaxes).toBe(3449.33);
  });

  test('Fortnightly total taxes should be $1,592.00', () => {
    expect(calculator.wages.fortnightly.totalTaxes).toBe(1592.00);
  });

  test('Weekly total taxes should be $796.00', () => {
    expect(calculator.wages.weekly.totalTaxes).toBe(796.00);
  });
});

describe('Salary 700.00 (daily), Super 9.50% (included)', () => {
  const calculator = new CalculatorModel();
  calculator.salary = 700.00;
  calculator.period = Period.DAILY;
  calculator.superannuation = 9.50;
  calculator.isSuperIncluded = true;
  calculator.isNonTaxFreeThreshold = false;

  test('Anual pay should be $113,846.05', () => {
    expect(calculator.wages.anually.pay).toBe(113846.05);
  });

  test('Monthly pay should be $9,487.17', () => {
    expect(calculator.wages.monthly.pay).toBe(9487.17);
  });

  test('Fortnightly pay should be $4,378.69', () => {
    expect(calculator.wages.fortnightly.pay).toBe(4378.69);
  });

  test('Weekly pay should be $2,189.35', () => {
    expect(calculator.wages.weekly.pay).toBe(2189.35);
  });

  test('Anual taxable income should be $166,210.05', () => {
    expect(calculator.wages.anually.taxableIncome).toBe(166210.05);
  });

  test('Monthly taxable income should be $13,850.84', () => {
    expect(calculator.wages.monthly.taxableIncome).toBe(13850.84);
  });

  test('Fortnightly taxable income should be $6,392.69', () => {
    expect(calculator.wages.fortnightly.taxableIncome).toBe(6392.69);
  });

  test('Weekly taxable income should be $3,196.35', () => {
    expect(calculator.wages.weekly.taxableIncome).toBe(3196.35);
  });

  test('Anual superannuation should be $15,789.95', () => {
    expect(calculator.wages.anually.superannuation).toBe(15789.95);
  });

  test('Monthly superannuation should be $1,315.83', () => {
    expect(calculator.wages.monthly.superannuation).toBe(1315.83);
  });

  test('Fortnightly superannuation should be $607.31', () => {
    expect(calculator.wages.fortnightly.superannuation).toBe(607.31);
  });

  test('Weekly superannuation should be $303.65', () => {
    expect(calculator.wages.weekly.superannuation).toBe(303.65);
  });

  test('Anual total taxes should be $52,364.00', () => {
    expect(calculator.wages.anually.totalTaxes).toBe(52364.00);
  });

  test('Monthly total taxes should be $4,363.67', () => {
    expect(calculator.wages.monthly.totalTaxes).toBe(4363.67);
  });

  test('Fortnightly total taxes should be $2,014.00', () => {
    expect(calculator.wages.fortnightly.totalTaxes).toBe(2014.00);
  });

  test('Weekly total taxes should be $1,007.00', () => {
    expect(calculator.wages.weekly.totalTaxes).toBe(1007.00);
  });
});
