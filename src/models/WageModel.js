class WageModel {
  constructor() {
    this._pay = 0;
    this._taxableIncome = 0;
    this._superannuation = 0;
    this._totalTaxes = 0;
    this._incomeTax = 0;
    this._medicare = 0;
    this._otherTaxesAndLevies = 0;
    this._taxOffets = 0;
  }

  get pay() {
    return(this._pay);
  }

  set pay(newPay) {
    this._pay = newPay;
  }

  get taxableIncome() {
    return(this._taxableIncome);
  }

  set taxableIncome(newTaxableIncome) {
    this._taxableIncome = newTaxableIncome;
  }

  get superannuation() {
   return(this._superannuation); 
  }

  set superannuation(newSuperannuation) {
    this._superannuation = newSuperannuation;
  }

  get totalTaxes() {
    return(this._totalTaxes);
  }

  set totalTaxes(newTotalTaxes) {
    this._totalTaxes = newTotalTaxes;
  }

  get incomeTax() {
    return(this._incomeTax);
  }

  set incomeTax(newIncomeTax) {
    this._incomeTax = newIncomeTax;
  }

  get medicare() {
    return(this._medicare);
  }

  set medicare(newMedicare) {
    this._medicare = newMedicare;
  }

  get otherTaxesAndLevies() {
    return(this._otherTaxesAndLevies);
  }

  set otherTaxesAndLevies(newOtherTaxesAndLevies) {
    this._otherTaxesAndLevies = newOtherTaxesAndLevies;
  }

  get taxOffsets() {
    return(this._taxOffsets);
  }

  set taxOffsets(newTaxOffsets) {
    this._taxOffsets = newTaxOffsets;
  }
}

export { WageModel };
