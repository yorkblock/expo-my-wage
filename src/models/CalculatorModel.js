import { WageModel } from './WageModel';

const Period = Object.freeze({
  ANUALLY: 0,
  MONTHLY: 1,
  FORTNIGHTLY: 2,
  WEEKLY: 3,
  DAILY: 4
});

class CalculatorModel {
  constructor() {
    this._salary = 0;
    this._period = Period.ANUALLY;
    this._superannuation = 0.0;
    this._isSuperIncluded = false;
    this._isBackpacker = false;
    this._isNonResident = false;
    this._isNonTaxFreeThreshold = false;
    this._isStudentLoan = false;
    this._isWithHoldTaxOffsets = false;

    this._wages = {
      anually: new WageModel(),
      monthly: new WageModel(),
      fortnightly: new WageModel(),
      weekly: new WageModel()
    };
  }

  get salary() {
    return(this._salary);
  }

  set salary(newSalary) {
    this._salary = newSalary;
    this.calculate();
  }

  get period() {
    return(this._period);
  }

  set period(newPeriod) {
    this._period = newPeriod;
    this.calculate();
  }

  get superannuation() {
    return(this._superannuation);
  }

  set superannuation(newSuperannuation) {
    this._superannuation = newSuperannuation;
    this.calculate();
  }

  get isSuperIncluded() {
    return(this._isSuperIncluded);
  }

  set isSuperIncluded(newIsSuperIncluded) {
    this._isSuperIncluded = newIsSuperIncluded;
    this.calculate();
  }

  get isBackpacker() {
    return(this._isBackpacker);
  }

  set isBackpacker(newIsBackpacker) {
    this._isBackpacker = newIsBackpacker;
    this.calculate();
  }

  get isNonResident() {
    return(this._isNonResident);
  }

  set isNonResident(newIsNonResident) {
    this._isNonResident = newIsNonResident;
    this.calculate();
  }

  get isNonTaxFreeThreshold() {
    return(this._isNonTaxFreeThreshold);
  }

  set isNonTaxFreeThreshold(newIsNonTaxFreeThreshold) {
    this._isNonTaxFreeThreshold = newIsNonTaxFreeThreshold;
    this.calculate();
  }

  get isStudentLoan() {
    return(this._isStudentLoan);
  }

  set isStudentLoan(newIsStudentLoan) {
    this._isStudentLoan = newIsStudentLoan;
    this.calculate();
  }

  get isWithHoldTaxOffsets() {
    return(this._isWithHoldTaxOffsets);
  }

  set isWithHoldTaxOffsets(newIsWithHoldTaxOffsets) {
    this._isWithHoldTaxOffsets = newIsWithHoldTaxOffsets;
    this.calculate();
  }

  get wages() {
    return(this._wages);
  }

  set wages(newWages) {
    this._wages = newWages;
    this.calculate();
  }

  calculate() {
    switch (this.period) {
      case Period.ANUALLY:
        this.calculateWith((this.salary / 4) / 13);
        break;

      case Period.MONTHLY:
        this.calculateWith((this.salary * 3) / 13);
        break;

      case Period.FORTNIGHTLY:
        this.calculateWith(this.salary * 2);
        break;

      case Period.WEEKLY:
        this.calculateWith(this.salary);
        break;

      case Period.DAILY:
        this.calculateWith(this.salary * 5);
        break;
    }
  }

  calculateWith(weeklySalary) {
    let superannuation = 0;
    let taxableIncome = 0;

    if (this.isSuperIncluded === true) {
      superannuation = (weeklySalary * this.superannuation) / (100 + this.superannuation);
      taxableIncome = weeklySalary - superannuation;
    } else {
      taxableIncome = weeklySalary;
      superannuation = (taxableIncome * this.superannuation) / 100;
    }
    const weeklyTaxWithold = this.calculateWeeklyTaxes(taxableIncome);

    this.updateWageDetails(this.wages.anually, taxableIncome, superannuation, weeklyTaxWithold, 13 * 4);
    this.updateWageDetails(this.wages.monthly, taxableIncome, superannuation, weeklyTaxWithold, 13 / 3);
    this.updateWageDetails(this.wages.fortnightly, taxableIncome, superannuation, weeklyTaxWithold, 2);
    this.updateWageDetails(this.wages.weekly, taxableIncome, superannuation, weeklyTaxWithold, 1);
  }

  calculateWeeklyTaxes(taxableIncome) {
    const limit = Number.MAX_SAFE_INTEGER;

    const taxFreeThresholds = [
      { limit: 355, coefficients: { a: 0, b: 0 } },
      { limit: 422, coefficients: { a: 0.1900, b: 67.4635 } },
      { limit: 528, coefficients: { a: 0.2900, b: 109.7327 } },
      { limit: 711, coefficients: { a: 0.2100, b: 67.4635 } },
      { limit: 1281, coefficients: { a: 0.3477, b: 165.4423 } },
      { limit: 1730, coefficients: { a: 0.3450, b: 161.9808 } },
      { limit: 3461, coefficients: { a: 0.3900, b: 239.8654 } },
      { limit: [limit], coefficients: { a: 0.4700, b: 516.7885 } }
    ];

    const nonTaxFreeThresholds = [
      { limit: 72, coefficients: { a: 0.1900, b: 0.1900 } },
      { limit: 361, coefficients: { a: 0.2342, b: 3.2130 } },
      { limit: 932, coefficients: { a: 0.3477, b: 44.2476 } },
      { limit: 1380, coefficients: { a: 0.3450, b: 41.7311 } },
      { limit: 3111, coefficients: { a: 0.3900, b: 103.8657 } },
      { limit: [limit], coefficients: { a: 0.4700, b: 352.7888 } }
    ];

    const thresholds = this.isNonTaxFreeThreshold ? nonTaxFreeThresholds : taxFreeThresholds;
    const weeklyTaxable = Math.floor(taxableIncome) + 0.99;
    const threshold = thresholds.find(threshold => weeklyTaxable < threshold.limit);
    const coefficients = threshold.coefficients;
    const taxWithold = coefficients.a * weeklyTaxable - coefficients.b;

    return Math.round(taxWithold);
  }

  updateWageDetails(wagePeriod, taxableIncome, superannuation, totalTaxes, coefficient) {
    wagePeriod.taxableIncome = taxableIncome * coefficient;
    wagePeriod.superannuation = superannuation * coefficient;
    wagePeriod.totalTaxes = totalTaxes * coefficient;
    wagePeriod.pay = wagePeriod.taxableIncome - wagePeriod.totalTaxes;

    wagePeriod.taxableIncome = this.roundTo2Decimals(wagePeriod.taxableIncome);
    wagePeriod.superannuation = this.roundTo2Decimals(wagePeriod.superannuation);
    wagePeriod.totalTaxes = this.roundTo2Decimals(wagePeriod.totalTaxes);
    wagePeriod.pay = this.roundTo2Decimals(wagePeriod.pay);
  }

  roundTo2Decimals(value) {
    return Math.round(value * 100 + Number.EPSILON) / 100;
  }

  dump() {
    console.log(this);
  }
}

export { CalculatorModel, Period };
