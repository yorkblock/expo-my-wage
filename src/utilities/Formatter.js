const formatAmount = (value) => {
  let formatted = value.toLocaleString('en-AU', {
    style: 'currency',
    currency: 'AUD',
    minimumFractionDigits: 0
  });
  return (formatted);
};

const formatPercentage = (value) => {
  let formatted = (value / 100).toLocaleString('en-AU', {
    style: 'percent',
    minimumFractionDigits: 2
  });
  return (formatted);
};

export { formatAmount, formatPercentage };
