import { Segmented } from './common/Segmented';

const SalarySuperSegmentType = Object.freeze({
  YEAR: 0,
  MONTH: 1,
  WEEK: 2,
  DAY: 3,
  SUPER: 4
});

class SalarySuperSegmented extends Segmented {
  componentDidMount() {
    super.componentDidMount();

    const { YEAR, MONTH, WEEK, DAY, SUPER } = SalarySuperSegmentType; 

    this.setState({
      segments: [
        { key: YEAR, title: 'Year', isSelected: true },
        { key: MONTH, title: 'Month', isSelected: false },
        { key: WEEK, title: 'Week', isSelected: false },
        { key: DAY, title: 'Day', isSelected: false },
        { key: SUPER, title: 'Super', isSelected: false },
      ]
    });
  }
}

export { SalarySuperSegmented, SalarySuperSegmentType };
