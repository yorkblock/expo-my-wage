import { DropDownMenu } from './common/DropDownMenu';

const TaxOptionType = Object.freeze({
  INCLUDE_SUPERANNUATION: 0,
  NO_TAX_FREE_THRESHOLD: 1
});

class TaxOptionsDropDownMenu extends DropDownMenu {
}

export { TaxOptionsDropDownMenu, TaxOptionType };
