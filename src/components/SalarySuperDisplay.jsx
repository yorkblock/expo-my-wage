import React from 'react';
import { View } from 'react-native';
import {
  Header,
  Display
} from './common';
import {
  formatAmount,
  formatPercentage
} from '../utilities/Formatter';

class SalarySuperDisplay extends React.Component {
  headers = () => {
    const { period, salary, superannuation } = this.props;
    const salaryHeader = period + ': ' + formatAmount(salary);
    const superannuationHeader = 'Super: ' + formatPercentage(superannuation);

    return ([
      { text: salaryHeader },
      { text: superannuationHeader },
      { text: '2019-20' }
    ]);
  }

  display = () => {
    const { showSuperannuation, superannuation, salary } = this.props;

    if (showSuperannuation) {
      return (formatPercentage(superannuation));
    }

    return (formatAmount(salary));
  }

  render() {
    return (
      <View>
        <Header titles={this.headers()} />
        <Display>
          {this.display()}
        </Display>
      </View>
    );
  }
}

export { SalarySuperDisplay };
