import React from 'react';
import { Animated } from 'react-native';
import {
  Card,
  Carrousel
} from './common';
import { formatAmount } from '../utilities/Formatter';

class ResultCarrousel extends React.Component {
  state = {
    fadeAnimation: new Animated.Value(0)
  };

  componentDidMount() {
    Animated.timing(
      this.state.fadeAnimation,
      {
        toValue: 1,
        duration: 750
      }
    ).start();
  }

  results = () => {
    const { anually, monthly, fortnightly, weekly } = this.props.result;

    return ({
      year: {
        title: 'Year',
        detailList: [
          { left: 'Pay', right: formatAmount(anually.pay) },
          { left: 'Taxable Income', right: formatAmount(anually.taxableIncome) },
          { left: 'Super', right: formatAmount(anually.superannuation) },
          { left: 'Total Taxes', right: formatAmount(anually.totalTaxes) }
        ]
      },
      month: {
        title: 'Month',
        detailList: [
          { left: 'Pay', right: formatAmount(monthly.pay) },
          { left: 'Taxable Income', right: formatAmount(monthly.taxableIncome) },
          { left: 'Super', right: formatAmount(monthly.superannuation) },
          { left: 'Total Taxes', right: formatAmount(monthly.totalTaxes) }
        ]
      },
      fortnight: {
        title: 'Fortnight',
        detailList: [
          { left: 'Pay', right: formatAmount(fortnightly.pay) },
          { left: 'Taxable Income', right: formatAmount(fortnightly.taxableIncome) },
          { left: 'Super', right: formatAmount(fortnightly.superannuation) },
          { left: 'Total Taxes', right: formatAmount(fortnightly.totalTaxes) }
        ]
      },
      week: {
        title: 'Week',
        detailList: [
          { left: 'Pay', right: formatAmount(weekly.pay) },
          { left: 'Taxable Income', right: formatAmount(weekly.taxableIncome) },
          { left: 'Super', right: formatAmount(weekly.superannuation) },
          { left: 'Total Taxes', right: formatAmount(weekly.totalTaxes) }
        ]
      }
    });
  };

  render() {
    const { year, month, fortnight, week } = this.results();

    return (
      <Animated.View style={{ flex: 1, opacity: this.state.fadeAnimation }}>
        <Carrousel>
          <Card
            title={year.title}
            detailList={year.detailList}
          />

          <Card
            title={month.title}
            detailList={month.detailList}
          />

          <Card
            title={fortnight.title}
            detailList={fortnight.detailList}
          />

          <Card
            title={week.title}
            detailList={week.detailList}
          />
        </Carrousel>
      </Animated.View>
    );
  }
}

export { ResultCarrousel };
