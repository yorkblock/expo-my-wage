import React from 'react';
import {
  StyleSheet,
  View,
  Text
} from 'react-native';

const TINT_COLOR = 'darkslateblue'; // TODO: This should be part of the props.

const Header = ({ titles }) => {
  const titlesList = titles.map((title, key) => (
    <Text key={key} style={styles.title}>
      {title.text}
    </Text>
  ));

  return (
    <View style={styles.container}>
      {titlesList}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 4,
    height: 26,
    backgroundColor: TINT_COLOR
  },
  title: {
    color: 'white'
  }
});

export { Header };
