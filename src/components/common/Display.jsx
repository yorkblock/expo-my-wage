import React from 'react';
import {
  View,
  Text,
  StyleSheet
} from 'react-native';

const TINT_COLOR = 'darkslateblue'; // TODO: This should be part of the props.

class Display extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.text}>
          {this.props.children}
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: 60,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    padding: 4,
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: TINT_COLOR
  },
  text: {
    textAlign: 'right',
    backgroundColor: 'white',
    fontSize: 22,
    color: TINT_COLOR
  }
});

export { Display };
