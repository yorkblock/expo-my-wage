import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity
} from 'react-native';
import Segment from './Segment';

class Segmented extends React.Component {
  state = { segments: [] };

  componentDidMount() {
    this.setState({ segments: this.props.segments });
  }

  onPress(segmentPressed) {
    const segments = this.state.segments.map(segment => ({
      key: segment.key,
      title: segment.title,
      isSelected: segmentPressed.key === segment.key
    }));

    this.setState({ segments });

    this.props.onSelect({ ...segmentPressed, isSelected: true });
  }

  renderSegments() {
    return this.state.segments.map(segment =>
      <Segment
        key={segment.key}
        title={segment.title}
        isSelected={segment.isSelected}
        onPress={() => this.onPress(segment)}
      />
    );
  }

  render() {
    return (
      <View style={styles.container}>
        {this.renderSegments()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    backgroundColor: 'white',
    padding: 0
  }
});

export { Segmented };
