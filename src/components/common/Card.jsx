import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Dimensions
} from 'react-native';

const TINT_COLOR = 'darkslateblue'; // TODO: This should be part of the props.
const { width } = Dimensions.get('window'); 

const Card = ({ title, detailList }) => {
  const detailsRendering = detailList.map((detail, key) => (
    <View style={styles.detailContainer} key={key}>

      <Text style={styles.leftDetail}>
        {detail.left}
      </Text>

      <Text style={styles.rightDetail}>
        {detail.right}
      </Text>

    </View>
  ));

  return (
    <View style={styles.container}>

      <View style={styles.titleContainer}>
        <Text style={styles.title}>{title}</Text>
      </View>

      <View style={styles.detailListContainer}>
        {detailsRendering}
      </View>

    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: width - 60,
    padding: 4,
    backgroundColor: 'white'
  },
  titleContainer: {
    padding: 4,
    height: 30,
    justifyContent: 'center',
    backgroundColor: TINT_COLOR,
    borderWidth: 1,
    borderColor: TINT_COLOR,
  },
  title: {
    textAlign: 'center',
    color: 'white'
  },
  detailListContainer: {
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: TINT_COLOR
  },
  detailContainer: {
    flexDirection: 'row',
    padding: 4,
    height: 26,
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: 'white'
  },
  leftDetail: {
    textAlign: 'left'
  },
  rightDetail: {
    textAlign: 'right' 
  }
});

export { Card };
