import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity
} from 'react-native';

const TINT_COLOR = 'darkslateblue'; // TODO: This should be part of the props.

const Segment = ({title, isSelected, onPress}) => {
  let selected = {};
  if (isSelected) {
    selected = {
      touchable: {
        backgroundColor: TINT_COLOR,
        borderColor: 'white'
      },
      text: {
        color: 'white'
      }
    };
  }

  return (
    <View style={styles.container}>

      <TouchableOpacity
        style={[ styles.touchable, selected.touchable ]}
        onPress={onPress}
      >

        <Text style={[ styles.title, selected.text ]}>
          {title}
        </Text>

      </TouchableOpacity>

    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 4,
    height: 32,
    backgroundColor: 'darkslateblue'
  },
  touchable: {
    flex:1,
    justifyContent: 'center',
    backgroundColor: 'white',
    borderColor: TINT_COLOR,
    borderWidth: 1
  },
  title: {
    color: TINT_COLOR,
    textAlign: 'center'
  }
});

export default Segment;

