import React from 'react';
import {
  StyleSheet,
  View,
  ScrollView,
  Dimensions
} from 'react-native';

const TINT_COLOR = 'darkslateblue'; // TODO: This should be part of the props.
const { width } = Dimensions.get('window'); 

class Carrousel extends React.Component {
  componentDidMount() {
    if (this.props.hide) {
      return;
    }
		setTimeout(() => { this.scrollView.scrollTo({x: -24}) }, 1)
	}

  render() {
    if (this.props.hide) {
      return (
        <View />
      );
    }

    return (
      <ScrollView
        ref={(scrollView) => { this.scrollView = scrollView; }}
        style={styles.container}
        showsHorizontalScrollIndicator={false}
        horizontal={true}
        decelerationRate={0}
        snapToInterval={width - 60}
        snapToAlignment='center'
        contentInset={{
          top: 0,
          left: 30,
          bottom: 0,
          right: 30
        }}
      >
        {this.props.children}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: { backgroundColor: 'white' }
});

export { Carrousel };

