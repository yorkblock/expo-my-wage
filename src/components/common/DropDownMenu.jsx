import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  Animated
} from 'react-native';
import { FontAwesome } from '@expo/vector-icons';

const TINT_COLOR = 'darkslateblue'; // TODO: This should be part of the props.

class DropDownMenu extends React.Component {
  state = {
    isCollapsed: true,
    animation: new Animated.Value(4)
  };

  toggle() {
    const { isCollapsed } = this.state;
    const { options } = this.props;
    const initial = isCollapsed ? 4 : options.length * styles.optionsIndividualContainer.height;
    const final = isCollapsed ? options.length * styles.optionsIndividualContainer.height : 4;

    this.setState({
      isCollapsed: !isCollapsed
    });

    this.state.animation.setValue(initial);
    Animated.spring(
      this.state.animation,
      {
        toValue: final
      }
    ).start();
  }

  renderHeader() {
    const { isCollapsed } = this.state;
    const { title } = this.props;
    const collapsedIcon = isCollapsed ? 'angle-double-up' : 'angle-double-down';

    return (
      <TouchableWithoutFeedback onPress={() => this.toggle()}>

        <View style={styles.headerContainer}>
          <Text style={styles.headerText}>
            {title}
          </Text>

          <FontAwesome name={collapsedIcon} size={20} color='white' />
        </View>

      </TouchableWithoutFeedback>
    );
  }

  onSelect(optionSelected) {
    this.props.onSelect({ ...optionSelected, isSelected: !optionSelected.isSelected });
  }

  renderOptions() {
    return (
      this.props.options.map(option => (
        <TouchableWithoutFeedback
          key={option.key}
          onPress={() => this.onSelect(option)}
        >

          <View style={styles.optionsIndividualContainer}>
            <Text>
              {option.title}
            </Text>

            <FontAwesome
              name={option.isSelected ? 'check-square' : 'square-o'}
              size={20}
              color={TINT_COLOR}
            />
          </View>

        </TouchableWithoutFeedback>
      ))
    );
  }

  renderOptionsContainer() {
    return (
      <Animated.View style={{ height: this.state.animation }}>
        <View style={styles.optionsContainer}>
          {this.renderOptions()}
        </View>
      </Animated.View>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        {this.renderHeader()}
        {this.renderOptionsContainer()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: 2,
    padding: 0,
    backgroundColor: 'white'
  },
  headerContainer: {
    flexDirection: 'row',
    padding: 4,
    height: 28,
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: TINT_COLOR
  },
  headerText: {
    color: 'white',
    fontSize: 15
  },
  optionsContainer: {
    overflow: 'hidden',
    borderWidth: 1,
    borderColor: TINT_COLOR
  },
  optionsIndividualContainer: {
    flexDirection: 'row',
    padding: 4,
    height: 28,
    justifyContent: 'space-between',
    backgroundColor: 'white'
  }
});

export { DropDownMenu };
