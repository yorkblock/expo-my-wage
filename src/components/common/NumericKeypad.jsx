import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableOpacity
} from 'react-native';
import Constants from 'expo-constants';

class NumericKeypad extends React.Component {
  render() {
    return (
      <View style={[ styles.container, { height: this.props.height } ]}>

        <FlatList
          style={{ flex: 1 }}
          scrollEnabled='false'
          numColumns='3'
          data={[
            { key: '1', title: '1' },
            { key: '2', title: '2' },
            { key: '3', title: '3' },
            { key: '4', title: '4' },
            { key: '5', title: '5' },
            { key: '6', title: '6' },
            { key: '7', title: '7' },
            { key: '8', title: '8' },
            { key: '9', title: '9' },
            { key: 'N', title: ' ' },
            { key: '0', title: '0' },
            { key: 'D', title: '\u232B' },
          ]}
          renderItem={({item}) => 
            <TouchableOpacity
              style={[ styles.button, { height: (this.props.height / 4) - 4 } ]}
              onPress={() => this.props.onPress(item.key)}
            >

              <Text style={styles.buttonTitle}>
                {item.title}
              </Text>

            </TouchableOpacity>
          }
        />

      </View>
    );
  }
}

const updateAmount = (value, key) => {
  if (key === 'D') {
    return(Math.floor(value /  10));
  } else {
    return((value * 10) + Number(key));
  }
};

const updatePercentage = (value, key) => {
  let tmp = value * 100;
  if (key === 'D') {
    return(Math.floor(tmp /  10) / 100);
  } else {
    return(((tmp * 10) + Number(key)) / 100);
  }
};

const styles = StyleSheet.create({
  container: {
    //flex:1,
    backgroundColor: 'darkslateblue'
  },
  button: { 
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'white',
    margin: 2
  },
  buttonTitle: {
    fontSize: 24,
    color: 'darkslateblue',
    fontWeight: 'bold',
    textAlign: 'center',
  }
});

export { NumericKeypad, updateAmount, updatePercentage };
